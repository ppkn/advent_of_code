import sys

class Scorer:
    def __init__(self, river):
        self.opener_list = []
        self.score = 0
        self.garbage = False
        self.escape = False
        self.garbage_count = 0

        self.river = river

    def open_curly(self, char):
        self.opener_list.append('{')
        self.score += len(self.opener_list)

    def close_curly(self, char):
        self.opener_list.pop()

    def comma(self, char):
        pass
    
    def start_garbage(self, char):
        self.garbage = True
    
    def check_garbage_end(self, char):
        if char == '>':
            self.garbage = False
        else:
            self.garbage_count += 1
    
    def escape_next_char(self):
        self.escape = True

    def cleanup_river(self):
        mapping = {
            '{': self.open_curly,
            '}': self.close_curly,
            ',': self.comma,
            '<': self.start_garbage,
        }

        while True:
            char = self.river.read(1)
            if self.escape:
                self.escape = False
            elif char == '!':
                self.escape_next_char()
            elif self.garbage:
                self.check_garbage_end(char)
            elif char == '\n':
                break
            else:
                mapping[char](char)

if __name__ == '__main__':
    def format_output(score, garbage_count):
        return ('='*8 + ' score ' + '='*8 + '\n' +
                f'{score}\n' +
                '\n' +
                '='*8 + ' garbage count ' + '='*8 + '\n' +
                f'{garbage_count}\n')
    
    river = sys.stdin
    scorer = Scorer(river)
    scorer.cleanup_river()
    print(format_output(scorer.score, scorer.garbage_count))
