from io import StringIO
from scorer import Scorer
examples = {
    '<>': 0,
    '<random characters>': 17,
    '<<<<>': 3,
    '<{!>}>': 2,
    '<!!>': 0,
    '<!!!>>': 0,
    '<{o"i!a,<{i<a>': 10
}

def test_garbage_count():
    for string, count in examples.items():
        river = StringIO(string + '\n')
        scorer = Scorer(river)
        scorer.cleanup_river()
        assert count == scorer.garbage_count