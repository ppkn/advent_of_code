from io import StringIO
from scorer import Scorer
examples = {
    '{}': 1,
    '{{{}}}': 6,
    '{{},{}}': 5,
    '{{{},{},{{}}}}': 16,
    '{<a>,<a>,<a>,<a>}': 1,
    '{{<ab>},{<ab>},{<ab>},{<ab>}}': 9,
    '{{<!!>},{<!!>},{<!!>},{<!!>}}': 9,
    '{{<a!>},{<a!>},{<a!>},{<ab>}}': 3
}

def test_score():
    for string, total in examples.items():
        river = StringIO(string + '\n')
        scorer = Scorer(river)
        scorer.cleanup_river()
        assert total == scorer.score
