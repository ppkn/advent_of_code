import sys

def checksum(two_dim):
    total = 0
    for row in two_dim:
        cols = [int(col.rstrip()) for col in row]
        cols = sorted(cols, reverse=True) # Now I know that I'm going big to small
        for i, big in enumerate(cols):
            for small in cols[i+1:]:
                if big % small == 0:
                    total += big / small
                    break
    return total

if __name__ == '__main__':
    lines = sys.stdin.readlines()
    tsv = [line.split('\t') for line in lines]
    print(checksum(tsv))
