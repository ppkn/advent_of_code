import sys

def checksum(two_dim):
    total = 0
    for row in two_dim:
        cols = [int(col.rstrip()) for col in row]
        total += max(cols) - min(cols)
    return total

if __name__ == '__main__':
    lines = sys.stdin.readlines()
    tsv = [line.split('\t') for line in lines]
    print(checksum(tsv))
