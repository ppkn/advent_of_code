import sys

def num_steps(jumps):
    cursor = 0
    steps = 0
    while cursor < len(jumps):
        old_val = jumps[cursor]
        if old_val >= 3:
            jumps[cursor] -= 1
        else:
            jumps[cursor] += 1
        cursor += old_val
        steps += 1
    return steps


if __name__ == '__main__':
    jumps = []
    for line in sys.stdin.readlines():
        jumps.append(int(line.rstrip()))
    print(num_steps(jumps))
