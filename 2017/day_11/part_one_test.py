from hex_grid import HexGrid

def test_one_direction():
    hex_grid = HexGrid()
    hex_grid.move('ne')
    hex_grid.move('ne')
    hex_grid.move('ne')
    assert hex_grid.distance() == 3

def test_there_and_back():
    hex_grid = HexGrid()
    hex_grid.move('ne')
    hex_grid.move('ne')
    hex_grid.move('sw')
    hex_grid.move('sw')
    assert hex_grid.distance() == 0

def test_l_shape():
    hex_grid = HexGrid()
    hex_grid.move('ne')
    hex_grid.move('ne')
    hex_grid.move('s')
    hex_grid.move('s')
    assert hex_grid.distance() == 2

def test_zig_zag():
    hex_grid = HexGrid()
    hex_grid.move('se')
    hex_grid.move('sw')
    hex_grid.move('se')
    hex_grid.move('sw')
    hex_grid.move('sw')
    assert hex_grid.distance() == 3
