from collections import namedtuple

Cell = namedtuple('Cell', 'x y z')

class HexGrid:
    def __init__(self):
        self.cursor = Cell(0, 0, 0)
    
    def move(self, direction):
        # ['n', 'ne', 'se', 's', 'sw', 'nw']
        if direction == 'n':
            x = self.cursor.x
            y = self.cursor.y + 1
            z = self.cursor.z - 1
            self.cursor = Cell(x, y, z)
        elif direction == 'ne':
            x = self.cursor.x + 1
            y = self.cursor.y
            z = self.cursor.z - 1
            self.cursor = Cell(x, y, z)
        elif direction == 'se':
            x = self.cursor.x + 1
            y = self.cursor.y - 1
            z = self.cursor.z
            self.cursor = Cell(x, y, z)
        elif direction == 's':
            x = self.cursor.x
            y = self.cursor.y - 1
            z = self.cursor.z + 1
            self.cursor = Cell(x, y, z)
        elif direction == 'sw':
            x = self.cursor.x - 1
            y = self.cursor.y
            z = self.cursor.z + 1
            self.cursor = Cell(x, y, z)
        elif direction == 'nw':
            x = self.cursor.x - 1
            y = self.cursor.y + 1
            z = self.cursor.z
            self.cursor = Cell(x, y, z)
        else:
            raise Exception(f'Invalid move: {direction}')
    
    def distance(self):
        return max([abs(coord) for coord in self.cursor])

if __name__ == '__main__':
    import sys
    stringy_directions = sys.argv[1]
    directions = stringy_directions.split(',')

    hex_grid = HexGrid()
    max_distance = 0
    for direction in directions:
        hex_grid.move(direction)
        if hex_grid.distance() > max_distance:
            max_distance = hex_grid.distance()
    
    print(max_distance)