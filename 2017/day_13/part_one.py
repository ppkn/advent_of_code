from firewall import Firewall

with open('input.txt', 'r') as f:
    firewall = Firewall(f)

firewall.send_packet()
print(f"Got a severity of {firewall.severity}")