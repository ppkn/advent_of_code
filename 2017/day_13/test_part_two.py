import pytest

@pytest.fixture
def firewall():
    from firewall import Firewall
    from io import StringIO
    test_input = ("0: 3\n"
                  "1: 2\n"
                  "4: 4\n"
                  "6: 4\n")
    test_io = StringIO(test_input)
    return Firewall(test_io)

def test_delay(firewall):
    firewall.send_packet(10)
    assert firewall.severity == 0

def test_min_delay(firewall):
    assert firewall.min_delay == 10