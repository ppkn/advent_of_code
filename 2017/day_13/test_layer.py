from layer import Layer
from operator import add, sub

def test_move_in_range():
    layer = Layer(3)
    layer.scanner = 1
    layer.move_scanner()
    assert layer.scanner == 2

def test_right_edge():
    layer = Layer(3)
    layer.scanner = 2
    assert layer.scanner_direction == add
    layer.move_scanner()
    assert layer.scanner_direction == sub
    assert layer.scanner == 1

def test_left_edge():
    layer = Layer(3)
    layer.scanner = 0
    layer.scanner_direction = sub
    layer.move_scanner()
    assert layer.scanner_direction == add
    assert layer.scanner == 1