# DO NOT RUN THIS CODE it is terrrrrrribly slow
from firewall import Firewall

with open('input.txt', 'r') as f:
    firewall = Firewall(f)

print(f"Minimum delay is {firewall.min_delay}")