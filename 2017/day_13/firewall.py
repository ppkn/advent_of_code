from layer import Layer
class Firewall:
    def __init__(self, iostream):
        self.layers = {} 
        self.max_depth = 0
        self.severity = 0
        self.caught = False

        self.read_file(iostream)
    
    def read_file(self, iostream):
        for line in iostream:
            depth, range_ = line.split(': ')
            depth, range_ = int(depth), int(range_)
            if depth > self.max_depth:
                self.max_depth = depth
            self.layers[depth] = Layer(range_)

    def move_scanners(self):
        for layer in self.layers.values():
            layer.move_scanner()
    
    def send_packet(self, delay=0, break_on_catch=False):
        self.severity = 0
        self.caught = False
        for _ in range(delay):
            self.move_scanners()
        for depth in range(self.max_depth + 1):
            if depth in self.layers:
                layer = self.layers[depth]
                if layer.scanner == 0:
                    self.severity += layer.range_ * depth
                    self.caught = True
                    if break_on_catch:
                        break
            self.move_scanners()

    def reset_scanners(self):
        for layer in self.layers.values():
            layer.reset_scanner()
    
    @property
    def min_delay(self):
        for delay in range(4_000_000):
            if delay % 10_000 == 0:
                print(f"Checked up to {delay} nanoseconds")
            self.send_packet(delay, break_on_catch=True)
            if self.severity == 0 and self.caught == False:
                return delay
            else:
                self.reset_scanners()
        else:
            raise Exception("Couldn't find a min delay in less than 4,000,000 nanoseconds")
