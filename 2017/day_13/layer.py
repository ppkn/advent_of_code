from operator import add, sub
class Layer:
    def __init__(self, range_):
        self.range_ = range_
        self.scanner = 0
        self.scanner_direction = add
    
    def _change_direction(self):
        direction_toggle = {add: sub, sub: add}
        self.scanner_direction = direction_toggle[self.scanner_direction]
    
    def move_scanner(self):
        next_pos = self.scanner_direction(self.scanner, 1)
        if next_pos in range(self.range_):
            self.scanner = next_pos
        else:
            self._change_direction()
            self.scanner = self.scanner_direction(self.scanner, 1)

    def reset_scanner(self):
        self.scanner = 0
        self.scanner_direction = add