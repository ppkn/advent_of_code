import pytest

@pytest.fixture
def firewall():
    from firewall import Firewall
    from io import StringIO
    test_input = ("0: 3\n"
                  "1: 2\n"
                  "4: 4\n"
                  "6: 4\n")
    test_io = StringIO(test_input)
    return Firewall(test_io)

def test_max_depth(firewall):
    assert firewall.max_depth == 6

def test_severity(firewall):
    firewall.send_packet()
    assert firewall.severity == 24
