firewall = {}
with open('input.txt', 'r') as f:
    for line in f:
        depth, range_ = line.split(': ')
        depth, range_ = int(depth), int(range_)
        firewall[depth] = range_

max_depth = max(firewall.keys())
for i in range(max_depth):
    if i not in firewall:
        firewall[i] = 0

for delay in range(4_000_000):
    if delay % 20000 == 0:
        print(f"Checked up to {delay} picoseconds")
    for picosecond in range(max_depth + 1):
        range_ = firewall[picosecond]
        if range_ == 0:
            continue
        if (delay + picosecond) % ((range_ - 1) * 2) == 0:
            break
    else:
        print(f"You should wait {delay} picoseconds")
        break