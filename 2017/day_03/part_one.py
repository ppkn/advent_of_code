import math

def num_to_coordinates(num):
    """Outputs the coordinates of the given number with 1 at the origin of (0,0)
    """

    coordinates = [(0,0), (1,0)]
    turtle = {
        "location": (1,0),
        "orientation": (1,0)
        }         
    rotate = {
        (1,0): (0,1),
        (0,1): (-1,0),
        (-1,0): (0,-1),
        (0,-1): (1,0)
        }
    offset = 1

    for i in range(3,num+1):
        turtle["location"] = coordinates[-1]
        left = rotate[turtle["orientation"]]
        left_spot = tuple(sum(pair) for pair in zip(turtle["location"],left)) # vector addition for spot to the left
        if (left_spot in coordinates):
            turtle["location"] = tuple(sum(pair) for pair in zip(turtle["location"],turtle["orientation"])) # vector addition for spot in front of turtle
            coordinates.append(turtle["location"])
        else:
            turtle["location"] = left_spot
            turtle["orientation"] = left
            coordinates.append(turtle["location"])

        # clean up the coordinates a little if we have a new square
        root = math.sqrt(i)
        if (math.floor(root) == root) and (root % 2 == 1): # this means we have a complete square
            offset = (root - 3) * 4 if root > 3 else 1 # I guess this works. Who knows why?
            offset = int(offset)
            coordinates = coordinates[offset:] # slice off up to the previous root

    return coordinates[-1]
