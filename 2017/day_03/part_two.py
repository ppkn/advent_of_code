import sys

def add_vectors(a, b):
    return tuple(sum(pair) for pair in zip(a, b))

def times_vectors(a, b):
    return tuple(pair[0] * pair[1] for pair in zip(a,b))

def rotate_left(pair):
    return (-pair[1], pair[0])

class Turtle:
    
    def __init__(self, location=(0,0), orientation=(0,-1)):
        self.location = location
        self.orientation = orientation
        self.rug = Rug()
        self.belly = 0

    def poop(self):
        self.rug.grow(self.location)
        self.belly = 1 if self.belly == 0 else self.belly
        self.rug.values.append(self.belly)
        self.belly = 0
 
    def gather_food(self):
        left = rotate_left(self.orientation)
        back_spot = add_vectors(self.location, times_vectors(self.orientation, (-1, -1)))
        back_left_spot = add_vectors(back_spot, left)
        left_spot = add_vectors(self.location, left)
        front_left_spot = add_vectors(add_vectors(self.location, self.orientation), left)
        for spot in [back_spot, back_left_spot, left_spot, front_left_spot]:
            if spot in self.rug.coordinates:
                coord_index = self.rug.coordinates.index(spot)
                self.belly += self.rug.values[coord_index]
        


    def move(self):
        left = rotate_left(self.orientation)
        left_spot = add_vectors(self.location, left)
        if left_spot not in self.rug.coordinates:
            self.orientation = left
        self.location = add_vectors(self.location, self.orientation)

class Rug:

    def __init__(self):
        self.coordinates = []
        self.values = []

    def grow(self, coords):
        if abs(coords[0]) == abs(coords[1]) and coords[0] > 0 and coords[1] < 0: # we can do some shrinking
            idx = 0
            while max(abs(point) for point in self.coordinates[idx]) < coords[0]:
                idx += 1
            print("Delete values to {}".format(self.values[idx]))
            self.coordinates = self.coordinates[idx:]
            self.values = self.values[idx:]
        self.coordinates.append(coords)



if __name__ == '__main__':
    yertle = Turtle()
    value = 0
    target = int(sys.argv[1])

    while value < target: # 312051
        yertle.gather_food()
        yertle.poop()
        yertle.move()
        value = yertle.rug.values[-1]
        print('{} : {}'.format(yertle.rug.values[-1], yertle.rug.coordinates[-1]))


    print('='*30 + '\n' + 'Final value: {}'.format(value))

