## Day One: Inverse Captcha
Part Two actually has the same functionality as part_one. If you'd like to use it for part one, just remove `part_one=False` from the `__main__` function.

### Usage
Pipe the challenge string into `part_two.py`. I used xclip to do this by copying the string and typing this:
```
xclip -0 | python part_two.py
```
