import sys

def inverse_captcha(challenge, part_one=True):
    """Returns the sum of all numbers in `challenge` that match either:
       - part_one: their right-hand neighbor
       - part_two: their neighbor halfway around the circle"""
    nums = [int(e) for e in list(challenge)]
    offset = 1 if part_one else len(nums) // 2
    total = 0
    for i, num in enumerate(nums):
        neighbor_index = (i + offset) % len(nums)
        if num == nums[neighbor_index]:
            total += num
    return total

if __name__ == '__main__':
   challenge = sys.stdin.readline().rstrip()
   print(inverse_captcha(challenge, part_one=False))
