import sys

def inverse_captcha(challenge):
    """Returns the sum of all numbers in `challenge` that match their right-hand neighbor"""
    nums = [int(e) for e in list(challenge)]
    total = 0
    for current, neighbor in zip(nums, nums[1:]):
        if current == neighbor:
            total += current
    if nums[-1] == nums[0]:
        total += nums[-1]
    return total


if __name__ == '__main__':
   challenge = sys.stdin.readline().rstrip()
   print(inverse_captcha(challenge))
