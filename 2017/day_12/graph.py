from collections import namedtuple
from itertools import combinations, filterfalse, tee
from functools import reduce
import random

Node = namedtuple('Node', 'id_ connections')

class Graph:
    def __init__(self):
        self.nodes = []
    
    def __getitem__(self, item):
        try:
            return next(group for group in self._groups if item in group)
        except StopIteration:
            raise KeyError(f"Could find a group with {item} in the graph")
        except AttributeError:
            raise AttributeError("This graph doesn't have any groups yet")
    
    def load_file(self, infile, num_nodes=None):
        """Load a graph from a file using AoC input format

        Args:
            infile (TextIO): A file/stream that is open for reading
            num_nodes (int): Number of nodes in the file. This helps with tracking
                groups while loading the file. If `None`, groups will be calculated
                after the graph has been loaded.
        """
        if num_nodes:
            self._groups = [{i} for i in range(num_nodes)]

        for line in infile:
            id_, connections = line.split(' <-> ')
            id_ = int(id_)
            connections = [int(c) for c in connections.split(', ')]
            if num_nodes:
                nodeset = set(connections) | {id_}
                related_groups = self._partition_groups(nodeset)
                joined_group = reduce(lambda a, b: a | b, related_groups)
                self._groups.append(joined_group)
            node = Node(id_, connections)
            self.nodes.append(node)
    
    def generate_random(self):
        num_nodes = 20
        num_connections = 12
        nodes = {i: set() for i in range(num_nodes)}
        all_connections = combinations(range(num_nodes), 2)
        connections = random.sample(list(all_connections), num_connections)
        for a, b in connections:
            nodes[a] |= {b}
            nodes[b] |= {a}
        self.nodes = [Node(k, list(v)) for k, v in nodes.items()]

    @property
    def groups(self):
        try:
            return self._groups
        except AttributeError:
            num_nodes = len(self.nodes)
            self._groups = [{i} for i in range(num_nodes)]
            for node in self.nodes:
                nodeset = set(node.connections) | {node.id_}
                related_groups = self._partition_groups(nodeset)
                joined_group = reduce(lambda a, b: a | b, related_groups)
                self._groups.append(joined_group)
            return self._groups
    
    def _partition_groups(self, nodeset):
        """Pulls the sets that intersect with `nodeset` out of groups and
        returns them.
        """
        g1, g2 = tee(self._groups)

        in_ = filterfalse(lambda x: x & nodeset, g1)
        out = filter(lambda x: x & nodeset, g2)
        self._groups = list(in_)
        return list(out)

if __name__ == '__main__':
    import sys

    graph = Graph()
    with open(sys.argv[1], 'r') as f:
        graph.load_file(f, num_nodes=2000)
    print(f'Size of group containing `0`: {len(graph[0])}')
    print(f'Number of groups: {len(graph.groups)}')
