from graph import Graph, Node
from io import StringIO
test_string = ('0 <-> 2\n'
               '1 <-> 1\n'
               '2 <-> 0, 3, 4\n'
               '3 <-> 2, 4\n'
               '4 <-> 2, 3\n')
graph = Graph()

def test_file_parsing():
    graph.load_file(StringIO(test_string))
    assert Node(2, [0, 3, 4]) in graph.nodes
    assert Node(4, [2, 3]) in graph.nodes

def test_groups():
    graph.load_file(StringIO(test_string), num_nodes=5)
    assert {0, 2, 3, 4} in graph.groups
    assert {1} in graph.groups
    assert len(graph.groups) == 2

def test_star_group():
    test_string = ('0 <-> 2\n'
                   '1 <-> 3, 4\n'
                   '2 <-> 0, 4\n'
                   '3 <-> 1\n'
                   '4 <-> 1, 2\n')
    graph.load_file(StringIO(test_string), num_nodes=5)
    assert len(graph.groups) == 1

def test_bigger_graph():
    test_string = ('0 <-> 7\n'
                   '1 <-> 5, 6\n'
                   '2 <-> 6\n'
                   '3 <-> 7\n'
                   '4 <-> 8\n'
                   '5 <-> 1\n'
                   '6 <-> 1, 2\n'
                   '7 <-> 0, 3\n'
                   '8 <-> 4, 9\n'
                   '9 <-> 8\n')
    graph.load_file(StringIO(test_string), num_nodes=10)
    assert len(graph.groups) == 3