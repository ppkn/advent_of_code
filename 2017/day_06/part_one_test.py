from part_one import count_cycles
from part_one import cycle

def test_part_one():
    assert count_cycles([0,2,7,0]) == 5
    assert cycle([0,2,7,0]) == [2,4,1,2]
    assert cycle([2,4,1,2]) == [3,1,2,3]
    assert cycle([3,1,2,3]) == [0,2,3,4]
