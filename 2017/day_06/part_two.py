def count_cycles(banks):
    seen_configs = []

    while banks not in seen_configs:
        seen_configs.append(banks.copy())
        banks = cycle(banks)

    # now we've gotten to the entrance for the infinite loop
    seen_configs = []
    counter = 0
    while banks not in seen_configs:
        seen_configs.append(banks.copy())
        banks = cycle(banks)
        counter += 1
    return counter

def cycle(banks):
    biggest = max(banks)
    biggest_i = banks.index(biggest)
    banks[biggest_i] = 0
    current = biggest_i
    while biggest > 0:
        current = (current + 1) % len(banks)
        banks[current] += 1
        biggest -= 1
    return banks

if __name__ == '__main__':
    import sys
    raw_text = sys.stdin.read()
    banks = [int(x) for x in raw_text.split("\t")]
    print(count_cycles(banks))
