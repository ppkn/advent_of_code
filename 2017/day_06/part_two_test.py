from part_two import count_cycles
from part_two import cycle

def test_part_two():
    assert count_cycles([0,2,7,0]) == 4
    assert cycle([0,2,7,0]) == [2,4,1,2]
    assert cycle([2,4,1,2]) == [3,1,2,3]
    assert cycle([3,1,2,3]) == [0,2,3,4]
