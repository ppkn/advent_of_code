import re
from collections import Counter
def find_root(tree):
    possible_nodes = []
    all_edges = []
    for node, (weight, edges) in tree.items():
        if len(edges) > 0:
            possible_nodes.append(node)
            all_edges.extend(edges)
    for node in possible_nodes:
        if node not in all_edges:
            return node

def build_tree(lines):
    tree = {}
    for line in lines:
        items = line.split(' -> ')
        name, weight = items[0].split()
        weight = weight.replace('(','').replace(')','')
        weight = int(weight)
        try:
            edges = items[1].split(', ')
        except IndexError:
            edges = []
        tree[name] = (weight, edges)
    return tree

def get_weight(tree, name):
    weight, edges = tree[name]
    if len(edges) == 0:
        return weight
    else:
        return weight + sum(get_weight(tree, edge) for edge in edges)

def wrong_weight(tree, name):
    branches = tree[name][1]
    odd_branch = odd_weight_out(tree, branches)
    if odd_branch:
        return wrong_weight(tree, odd_branch)
    else:
        return (name, tree[name][0])

def odd_weight_out(tree, branches):
    weights = [get_weight(tree, branch) for branch in branches]
    for i, n in Counter(weights).items():
        if n == 1:
            return branches[weights.index(i)]
    else:
        return None

if __name__ == '__main__':
    import sys

    lines = sys.stdin.readlines()
    lines = [line.rstrip() for line in lines]
    tree = build_tree(lines)
    root = find_root(tree)
    branches = tree[root][1]
    print(root, branches)
    print(wrong_weight(tree, root))
