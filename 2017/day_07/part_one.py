def find_root(dag):
    possible_nodes = []
    all_edges = []
    for node, edges in dag.items():
        if len(edges) > 0:
            possible_nodes.append(node)
            all_edges.extend(edges)
    print(all_edges)
    for node in possible_nodes:
        if node not in all_edges:
            return node

def build_dag(lines):
    dag = {}
    for line in lines:
        items = line.split(' -> ')
        name = items[0].split()[0]
        try:
            edges = items[1].split(', ')
        except IndexError:
            edges = []
        dag[name] = edges
    return dag

if __name__ == '__main__':
    import sys

    lines = sys.stdin.readlines()
    lines = [line.rstrip() for line in lines]
    dag = build_dag(lines)
    print(find_root(dag))
