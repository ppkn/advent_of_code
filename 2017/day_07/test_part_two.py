from part_two import build_tree
from part_two import get_weight

test_lines = ("pbga (66)\n"
              "xhth (57)\n"
              "ebii (61)\n"
              "havc (66)\n"
              "ktlj (57)\n"
              "fwft (72) -> ktlj, cntj, xhth\n"
              "qoyq (66)\n"
              "padx (45) -> pbga, havc, qoyq\n"
              "tknk (41) -> ugml, padx, fwft\n"
              "jptl (61)\n"
              "ugml (68) -> gyxo, ebii, jptl\n"
              "gyxo (61)\n"
              "cntj (57)")

test_tree = {'cntj': (57, []),
             'ebii': (61, []),
             'fwft': (72, ['ktlj', 'cntj', 'xhth']),
             'gyxo': (61, []),
             'havc': (66, []),
             'jptl': (61, []),
             'ktlj': (57, []),
             'padx': (45, ['pbga', 'havc', 'qoyq']),
             'pbga': (66, []),
             'qoyq': (66, []),
             'tknk': (41, ['ugml', 'padx', 'fwft']),
             'ugml': (68, ['gyxo', 'ebii', 'jptl']),
             'xhth': (57, [])}
