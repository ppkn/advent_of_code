from collections import namedtuple
def parse_line(line):
    vals, condition = line.split(' if ')
    register, inc_dec, amount = vals.split()
    condition = stringify_register(condition)
    Instruction = namedtuple('Instruction', ['register', 'inc_dec', 'amount', 'condition'])
    return Instruction(register, inc_dec, int(amount), condition)

def stringify_register(condition):
    reg, rest = condition.split(' ', 1)
    reg = "local_reg['{}']".format(reg)
    return ' '.join([reg, rest])

def load_registers(instructions):
    return {instruction.register: 0 for instruction in instructions}

def evaluate_instruction(register, instruction):
    local_reg = register.copy()
    if eval(instruction.condition):
        if instruction.inc_dec == 'inc':
            local_reg[instruction.register] += instruction.amount
        else:
            local_reg[instruction.register] -= instruction.amount
    return local_reg

if __name__ == '__main__':
    import sys
    lines = sys.stdin.readlines()
    lines = [line.rstrip() for line in lines]
    instructions = [parse_line(line) for line in lines]
    regs = load_registers(instructions)
    for instruction in instructions:
        regs = evaluate_instruction(regs, instruction)
    print(max(x for x in regs.values()))
