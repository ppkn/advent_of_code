from rope import Rope

def test_twist_edge_case():
    rope = Rope(5)
    rope.cursor = 3
    rope.twist(3)
    assert rope.data == [3, 1, 2, 0, 4]

def test_knot_tying():
    rope = Rope(5)
    rope.tie_knot([3, 4, 1, 5, 0])
    assert rope.data == [3, 4, 2, 1, 0]

def test_long_knot_tying():
    rope = Rope(16)
    rope.tie_knot([6, 11, 10, 4, 7])
    assert rope.data == [7, 8, 11, 12, 13, 3, 2, 1, 0, 5, 15, 14, 10, 9, 4, 6]