class Rope:
    def __init__(self, size):
        self.data = list(range(size))
        self.cursor = 0
        self.skip_size = 0
    
    def twist(self, length):
        start = self.cursor
        end = (self.cursor + length) % len(self.data)
        if end > start:
            self.data[start:end] = reversed(self.data[start:end])
        elif length == 0:
            pass
        else:
            tail_length = len(self.data) - start
            head_length = length - tail_length
            sequence = self.data[start:] + self.data[:end]
            reversed_sequence = sequence[::-1]
            if head_length == 0:
                self.data[start:] = reversed_sequence[:tail_length]
            elif tail_length == 0:
                self.data[:end] = reversed_sequence[-head_length:]
            else:
                self.data[start:] = reversed_sequence[:tail_length]
                self.data[:end] = reversed_sequence[-head_length:]
    
    def move_cursor(self, length):
        movement = length + self.skip_size
        self.cursor = (self.cursor + movement) % len(self.data)
        self.skip_size += 1
    
    def tie_knot(self, lengths):
        for i, length in enumerate(lengths):
            self.twist(length)
            self.move_cursor(length)

if __name__ == '__main__':
    import sys
    from functools import reduce
    stringy_lengths = sys.argv[1]
    lengths = list(bytearray(stringy_lengths, 'ascii'))
    lengths += [17, 31, 73, 47, 23]


    rope = Rope(256)
    for _ in range(64):
        rope.tie_knot(lengths)
    sparse_hash = rope.data
    two_dim = [sparse_hash[i:i+16] for i in range(0, len(sparse_hash), 16)]
    dense_hash = [reduce(lambda x,y: x ^ y, block) for block in two_dim]
    digest = ''.join([hex(block)[2:] for block in dense_hash])
    print(digest)
