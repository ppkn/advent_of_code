import sys

def is_valid(password):
    words = password.split()
    return len(set(words)) == len(words)


if __name__ == '__main__':
    valid_count = 0
    total = 0
    for line in sys.stdin.readlines():
        if is_valid(line.rstrip()):
            valid_count += 1
        total += 1
    print('{} of {} passwords are valid.'.format(valid_count, total))
