import sys
from collections import Counter

def is_valid(password):
    unique_words = []
    words = password.split()
    for word in words:
        for check_word in unique_words:
            if Counter(word) == Counter(check_word):
                return False
        unique_words.append(word)
    return True

if __name__ == '__main__':
    valid_count = 0
    total = 0
    for line in sys.stdin.readlines():
        if is_valid(line.rstrip()):
            valid_count += 1
        total += 1
    print('{} of {} passwords are valid.'.format(valid_count, total))
