passes = File.foreach("input", :chomp => true).map { |boarding_pass| boarding_pass.tr("BFRL", "1010").to_i(2) }
puts passes.max
puts passes.reject { |pass| passes.include?(pass + 1) && passes.include?(pass - 1) }
