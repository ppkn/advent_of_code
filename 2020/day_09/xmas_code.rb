class XmasCode
  attr_accessor :cursor,
                :sequence,
                :vulnerability_position

  def initialize(sequence)
    @sequence = sequence
    @cursor = 0
  end

  def encryption_weakness
    vulnerability = first_vulnerability
    first = vulnerability_position - 1
    last = vulnerability_position - 1
    possible_solution = 0
    contiguous_numbers = []
    until (possible_solution == vulnerability)
      contiguous_numbers = sequence[first..last]
      possible_solution = contiguous_numbers.sum
      if possible_solution < vulnerability
        first -= 1
      elsif possible_solution > vulnerability
        last = last - 1
        first = last
      end
      break if first < 0
    end
    contiguous_numbers.max + contiguous_numbers.min
  end

  def first_vulnerability
    self.cursor = 25
    loop do
      range = (cursor - 25)...cursor
      break unless sequence[range].combination(2).any? { |combo| combo.sum == sequence[cursor] }
      self.cursor += 1
    end
    answer = sequence[cursor]
    self.vulnerability_position = cursor
    self.cursor = 0
    answer
  end
end
