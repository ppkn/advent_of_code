class Passport
  def initialize(raw_text)
    @text = raw_text
  end

  def birth_year_valid?
    fields["byr"].to_i.between?(1920, 2002)
  end

  def expiration_year_valid?
    fields["eyr"].to_i.between?(2020, 2030)
  end

  def eye_color_valid?
    %w(amb blu brn gry grn hzl oth).include?(fields["ecl"])
  end

  def field_names
    @field_names ||= fields.keys
  end

  def fields
    @fields ||= @text.scan(/(byr|iyr|eyr|hgt|hcl|ecl|pid):(\S+)/).to_h
  end

  def fields_valid?
    required_fields = %w(byr iyr eyr hgt hcl ecl pid)
    (required_fields & field_names).size == required_fields.size
  end

  def hair_color_valid?
    /^#[0-9a-f]{6}$/ =~ fields["hcl"]
  end

  def height_valid?
    unit = fields["hgt"].chars.last(2).join
    value = fields["hgt"].chomp(unit).to_i
    if unit == "cm"
      value.between?(150, 193)
    elsif unit == "in"
      value.between?(59, 76)
    else
      false
    end
  end

  def issue_year_valid?
    fields["iyr"].to_i.between?(2010, 2020)
  end

  def passport_id_valid?
    /^[0-9]{9}$/ =~ fields["pid"]
  end

  def valid?
    required_fields = %w(byr iyr eyr hgt hcl ecl pid)
    return false unless (required_fields & field_names).size == required_fields.size
    (
      fields_valid? &&
      birth_year_valid? &&
      issue_year_valid? &&
      expiration_year_valid? &&
      height_valid? &&
      hair_color_valid? &&
      eye_color_valid? &&
      passport_id_valid?
    )
  end
end

passports = File.read("input").split(/^$/).map { |passport_text| Passport.new(passport_text) }
puts passports.count(&:valid?)