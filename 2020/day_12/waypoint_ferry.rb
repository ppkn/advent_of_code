class WaypointFerry
  Instruction = Struct.new(:operation, :value)
  INSTRUCTION_PROCS = {
    "N" => Proc.new { |value| @waypoint += value * 1i },
    "S" => Proc.new { |value| @waypoint += value * -1i },
    "E" => Proc.new { |value| @waypoint += value },
    "W" => Proc.new { |value| @waypoint += -value },
    "L" => Proc.new { |value| @waypoint *= (1i**(value/90)) },
    "R" => Proc.new { |value| @waypoint *= ((-1i)**(value/90)) },
    "F" => Proc.new { |value| @position += @waypoint * value },
  }

  attr_reader :position, :waypoint

  def initialize
    @position = 0i
    @waypoint = 10+1i
  end

  def read_instructions(instruction_input)
    @instructions = instruction_input.map do |instruction|
      operation = instruction[0]
      value = instruction[1..-1].to_i
      Instruction.new(operation, value)
    end
  end

  def navigate
    @instructions.each do |instruction, step|
      instance_exec(instruction.value, &INSTRUCTION_PROCS[instruction.operation])
    end
  end
end
