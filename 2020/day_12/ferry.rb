class Ferry
  Instruction = Struct.new(:operation, :value)
  attr_accessor :position, :direction
  def initialize
    @position = [0, 0]
    @direction = "E"
  end

  def read_instructions(instruction_input)
    @instructions = instruction_input.map do |instruction|
      operation = instruction[0]
      value = instruction[1..-1].to_i
      Instruction.new(operation, value)
    end
  end

  def go(direction, value)
    case direction
    when "N"
      @position[0] += value
    when "S"
      @position[0] -= value
    when "E"
      @position[1] += value
    when "W"
      @position[1] -= value
    end
  end

  def navigate
    @instructions.each do |instruction|
      case instruction.operation
      when "F"
        go(@direction, instruction.value)
      when "L"
        turn_left(instruction.value)
      when "R"
        turn_right(instruction.value)
      else
        go(instruction.operation, instruction.value)
      end
    end
  end

  def turn_left(value)
    counter_clockwise = ["W", "S", "E", "N"]
    number_of_times = value / 90
    current_index = counter_clockwise.index(@direction)
    next_index = current_index + number_of_times
    next_index = next_index % counter_clockwise.size
    @direction = counter_clockwise[next_index]
  end

  def turn_right(value)
    clockwise = ["E", "S", "W", "N"]
    number_of_times = value / 90
    current_index = clockwise.index(@direction)
    next_index = current_index + number_of_times
    next_index = next_index % clockwise.size
    @direction = clockwise[next_index]
  end
end
