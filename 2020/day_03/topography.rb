class Topography
  def initialize(puzzle_input)
    @grid = puzzle_input.split("\n").map { |row| row.split("") }
  end

  def slope_intersections(delta_x, delta_y)
    current_x = 0
    (delta_y...@grid.size).step(delta_y).count do |row|
      current_x = (current_x + delta_x) % @grid.first.size
      @grid[row][current_x] == '#'
    end
  end
end

map = Topography.new(File.read("input"))
puts map.slope_intersections(3, 1)

intersections = [[1, 1], [3, 1], [5, 1], [7, 1], [1, 2]].map do |delta_x, delta_y|
  map.slope_intersections(delta_x, delta_y)
end
puts intersections.reduce(&:*)
