class ComputerV2
  attr_accessor :instructions,
                :mask,
                :memory
  def initialize(program_io)
    @instructions = []
    @memory = {}
    program_io.each_line do |line|
      load_instruction(line)
    end
  end

  def memory_sum
    memory.values.sum
  end

  def run
    instructions.each(&:run)
  end

private

  MEMORY_REGEX = /mem\[(\d+)\] = (\d+)/

  def load_instruction(line)
    type = line.start_with?("mask") ? :mask : :memory

    if type == :mask
      _, mask = line.split(" = ")
      @instructions << MaskInstruction.new(mask.chomp, self)
    else
      address = line[MEMORY_REGEX, 1].to_i
      value = line[MEMORY_REGEX, 2].to_i
      @instructions << MemoryInstruction.new(address, value, self)
    end
  end

  class MaskInstruction
    attr_accessor :type, :value

    def initialize(value, computer)
      @value = value
      @computer = computer
    end

    def run
      @computer.mask = value
    end
  end

  class MemoryInstruction
    attr_accessor :type, :address, :value, :addresses

    def initialize(address, value, computer)
      @addresses = []
      @address = address
      @value = value
      @computer = computer
    end

    def run
      mask_address
      set_memory
    end

  private

    def new_address_template
      @new_address_template ||= begin
        binary_address = address.to_s(2).rjust(36, "0")
        new_address_template = ""
        (0..35).each do |i|
          if @computer.mask[i] == "0"
            new_address_template << binary_address[i]
          else
            new_address_template << @computer.mask[i]
          end
        end
        new_address_template
      end
    end

    def mask_address
      number_exes = new_address_template.count("X")
      binary_replacements = (0...2**number_exes).map { |num| num.to_s(2).rjust(number_exes, "0") }

      binary_replacements.each do |replacement|
        new_address = new_address_template.dup

        replacement.chars.each do |digit|
          new_address.sub!("X", digit)
        end
        @addresses << new_address.to_i(2)
      end
    end

    def set_memory
      addresses.each do |address|
        @computer.memory[address] = value
      end
    end
  end
end
