class Computer
  attr_accessor :instructions,
                :mask,
                :memory
  def initialize(program_io)
    @instructions = []
    @memory = {}
    program_io.each_line do |line|
      load_instruction(line)
    end
  end

  def memory_sum
    memory.values.sum
  end

  def run
    instructions.each(&:run)
  end

private

  MEMORY_REGEX = /mem\[(\d+)\] = (\d+)/

  def load_instruction(line)
    type = line.start_with?("mask") ? :mask : :memory

    if type == :mask
      _, mask = line.split(" = ")
      @instructions << MaskInstruction.new(mask.chomp, self)
    else
      address = line[MEMORY_REGEX, 1]
      value = line[MEMORY_REGEX, 2].to_i
      @instructions << MemoryInstruction.new(address, value, self)
    end
  end

  class MaskInstruction
    attr_accessor :type, :value

    def initialize(value, computer)
      @value = value
      @computer = computer
    end

    def run
      @computer.mask = value
    end
  end

  class MemoryInstruction
    attr_accessor :type, :address, :value

    def initialize(address, value, computer)
      @address = address
      @value = value
      @computer = computer
    end

    def run
      mask_value
      set_memory
    end

  private

    def mask_value
      binary_value = value.to_s(2).rjust(36, "0")
      new_value = ""
      (0..35).each do |i|
        if @computer.mask[i] != "X"
          new_value << @computer.mask[i]
        else
          new_value << binary_value[i]
        end
      end
      @value = new_value.to_i(2)
    end

    def set_memory
      @computer.memory[address] = value
    end
  end
end
