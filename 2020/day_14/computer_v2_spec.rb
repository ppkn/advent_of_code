require "pry"
require "rspec"
require "stringio"

require "./computer_v2"

describe ComputerV2 do
  let(:computer) { ComputerV2.new(input) }

  context "when the input is the website's example" do
    let(:input) {
      StringIO.new(
        <<~INPUT
          mask = 000000000000000000000000000000X1001X
          mem[42] = 100
          mask = 00000000000000000000000000000000X0XX
          mem[26] = 1
        INPUT
      )
    }

    it "produces a sum of 208" do
      computer.run
      expect(computer.memory_sum).to eq(208)
    end
  end
end