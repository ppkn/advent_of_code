require "rspec"
require "stringio"

require "./computer"

describe Computer do
  let(:computer) { Computer.new(input) }

  context "when the input is the website's example" do
    let(:input) {
      StringIO.new(
        <<~INPUT
          mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
          mem[8] = 11
          mem[7] = 101
          mem[8] = 0
        INPUT
      )
    }

    it "produces a sum of 165" do
      computer.run
      expect(computer.memory_sum).to eq(165)
    end
  end
end
