class Luggage
  attr_accessor :directly_contained_by, :contains, :name, :bag_counts
  def initialize(line)
    @name = line.split("bags").first.strip
    @contains = line.scan(/\d ([a-z ]+) bags?/).flatten
    @bag_counts = line.scan(/(\d) ([a-z ]+) bags?/).each_with_object({}) do |match, bag_counts|
      number, name = match
      bag_counts[name] = number.to_i
    end
    @directly_contained_by = Set.new
  end
end

$luggages = File.foreach("input").each_with_object({}) do |line, luggages|
  luggage = Luggage.new(line)
  luggages[luggage.name] = luggage
end

$luggages.each do |parent_name, luggage|
  luggage.contains.each do |child_name|
    $luggages[child_name].directly_contained_by << parent_name
  end
end

def ancestors(name)
  luggage = $luggages[name]
  luggage.directly_contained_by | Set.new(luggage.directly_contained_by.map { |parent_name| ancestors(parent_name) }).flatten
end

def number_of_children(name)
  total = 0
  luggage = $luggages[name]
  luggage.bag_counts.each do |name, count|
    total += count + (count * number_of_children(name))
  end
  total
end

puts ancestors("shiny gold").size
puts number_of_children("shiny gold")
