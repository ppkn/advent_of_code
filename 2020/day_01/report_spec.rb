require "./report.rb"
RSpec.describe Report do
  describe "#check" do
    subject { described_class.new(input).check }
    context "when expenses don't contain 2020 pair" do
        let(:input) {
          <<~INPUT
            1721
            979
            366
            675
            1456
          INPUT
        }
      it "raises an error" do
        expect { subject }.to raise_error(ArgumentError)
      end
    end

    context "with the site-given example" do
        let(:input) {
          <<~INPUT
            1721
            979
            366
            299
            675
            1456
          INPUT
        }
      it { is_expected.to eq 514579 }
    end
  end
end