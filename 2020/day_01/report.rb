class Report
  def initialize(report_text)
    @expenses = report_text.split("\n").map(&:to_i)
  end

  def check(n_pairs = 2)
    @expenses.permutation(n_pairs) do |pair|
      if pair.sum == 2020
        return pair.reduce(:*)
      end
    end
    raise ArgumentError, "No 2020 pair found"
  end
end

input = File.read("input")
puts Report.new(input).check
puts Report.new(input).check(3)
