class Passwords
  def initialize(password_strings)
    @passwords = password_strings.map { |password_string| Password.new(password_string) }
  end

  def valid_count
    @passwords.count(&:valid?)
  end

  class Password
    def initialize(input_string)
      criteria, @password_text = input_string.split(":").map(&:strip)
      @min, @max, @required_char = criteria.split(/\s|\-/)
      @min = @min.to_i
      @max = @max.to_i
    end

    def letter_count
      @password_text.count(@required_char)
    end

    def old_valid?
      letter_count >= @min && letter_count <= @max
    end

    def valid?
      (@password_text[@min - 1] == @required_char) ^ (@password_text[@max - 1] == @required_char)
    end
  end
end


password_strings = File.read("input").split("\n")
passwords = Passwords.new(password_strings)

puts passwords.valid_count
