declarations = File.read("input").split(/^$/)
puts declarations.map { |declaration| Set.new(declaration.chars).size - 1 }.sum
sizes = declarations.map do |group|
  intersection = group.strip.split("\n").map { |person| Set.new(person.chars) }.reduce(&:&)
  intersection.size
end

puts sizes.sum
