require 'pry'
class Lobby
  attr_reader :seats
  def initialize(seat_input)
    @seats = {}
    seat_input.each_with_index do |row, row_index|
      row.each_with_index do |seat, column_index|
        next if seat == "."
        @seats[[row_index, column_index]] = Seat.new(seat)
      end
    end
    @last_row = @seats.keys.map(&:first).max
    @last_column = @seats.keys.map(&:last).max
  end

  def neighbors(row, column)
    neighbors = []
    offsets = [
      [-1, -1],
      [-1,  0],
      [-1,  1],
      [ 0, -1],
      [ 0,  1],
      [ 1, -1],
      [ 1,  0],
      [ 1,  1]
    ]
    offsets.each do |row_offset, column_offset|
      neighbor_row = row + row_offset
      neighbor_column = column + column_offset
      next unless @seats.key?([neighbor_row, neighbor_column])
      neighbors << @seats[[neighbor_row, neighbor_column]]
    end
    neighbors
  end

  def occupied
    Set.new(seats.select { |_, seat| seat.occupied? })
  end

  def seats_to_toggle
    seats.select do |(row, column), seat|
      if seat.occupied?
        neighbors(row, column).count(&:occupied?) >= 4
      else
        neighbors(row, column).none?(&:occupied?)
      end
    end
  end

  def stabilize
    steps_taken = 0
    last_occupied = nil
    until (last_occupied == occupied)
      last_occupied = occupied
      step
      steps_taken += 1
    end
    steps_taken
  end

  def stabilize_visually
    steps_taken = 0
    last_occupied = nil
    until (last_occupied == occupied)
      last_occupied = occupied
      step_visually
      steps_taken += 1
    end
    steps_taken
  end

  def step
    seats_to_toggle.each { |_, seat| seat.toggle }
  end

  def step_visually
    visual_seats_to_toggle.each { |_, seat| seat.toggle }
  end

  def visual_neighbors(row, column)
    neighbors = []
    offsets = [
      [-1, -1],
      [-1,  0],
      [-1,  1],
      [ 0, -1],
      [ 0,  1],
      [ 1, -1],
      [ 1,  0],
      [ 1,  1]
    ]
    offsets.each do |row_offset, column_offset|
      neighbor_row = row
      neighbor_column = column
      while inside_lobby?(neighbor_row, neighbor_column)
        neighbor_row += row_offset
        neighbor_column += column_offset
        if @seats.key?([neighbor_row, neighbor_column])
          neighbors << @seats[[neighbor_row, neighbor_column]]
          break
        end
      end
    end
    neighbors
  end

  def visual_seats_to_toggle
    @seats.select do |(row, column), seat|
      if seat.occupied?
        visual_neighbors(row, column).count(&:occupied?) >= 5
      else
        visual_neighbors(row, column).none?(&:occupied?)
      end
    end
  end

private

  def inside_lobby?(row, column)
    row.between?(0, @last_row) && column.between?(0, @last_column)
  end

  class Seat
    def initialize(character_representation)
      @occupied = (character_representation == "#")
    end

    def occupied?
      @occupied
    end

    def toggle
      @occupied = !@occupied
    end
  end
end
