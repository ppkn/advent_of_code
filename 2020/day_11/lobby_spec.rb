require_relative 'lobby'

RSpec.describe Lobby do
  let(:input) {
    <<~INPUT
      L.LL.LL.LL
      LLLLLLL.LL
      L.L.L..L..
      LLLL.LL.LL
      L.LL.LL.LL
      L.LLLLL.LL
      ..L.L.....
      LLLLLLLLLL
      L.LLLLLL.L
      L.LLLLL.LL
    INPUT
  }
  let(:lobby) { Lobby.new(input.split("\n").map(&:chars)) }

  describe "#initialize" do
    it "creates the correct number of seats" do
      expect(lobby.seats.size).to eq(71)
    end

    it "creates seats in the correct location" do
      expect(lobby.seats).to have_key([0, 0])
      expect(lobby.seats).to have_key([0, 5])
      expect(lobby.seats).to have_key([0, 9])
      expect(lobby.seats).to have_key([6, 2])

      expect(lobby.seats).to_not have_key([6, 0])
      expect(lobby.seats).to_not have_key([6, 1])
      expect(lobby.seats).to_not have_key([9, 1])
    end
  end

  describe "#neighbors" do
    it "returns the right number of neighbors" do
      expect(lobby.neighbors(0, 0).size).to eq(2)
      expect(lobby.neighbors(6, 2).size).to eq(5)
      expect(lobby.neighbors(8, 3).size).to eq(8)
    end
  end

  describe "#seats_to_toggle" do
    context "when all seats are empty" do
      it "returns all seats" do
        expect(lobby.seats_to_toggle.size).to eq(lobby.seats.size)
      end
    end
  end

  describe "#visual_neighbors" do
    let(:input) {
      <<~INPUT
        .......#.
        ...#.....
        .#.......
        .........
        ..#L....#
        ....#....
        .........
        #........
        ...#.....
      INPUT
    }

    it "sees the right number of neighbors" do
      expect(lobby.visual_neighbors(4, 3).size).to eq(8)
    end

    it "sees the right neighbors" do
      expect(lobby.visual_neighbors(4, 3)).to include(lobby.seats[[4,8]])
      expect(lobby.visual_neighbors(4, 3)).to include(lobby.seats[[2,1]])
    end
  end
end
