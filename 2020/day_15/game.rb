# Game
#   starting_numbers
#   spoken_numbers
#     last turn spoken
#     first_time?
class Game
  def initialize(input_string)
    @starting_numbers = input_string.split(",").map(&:to_i)
  end

  def play(number_of_rounds)
    spoken_numbers = Hash.new { |hash, key| hash[key] = SpokenNumber.new(key) }
    previous_number = nil
    if number_of_rounds > @starting_numbers.size
      current_turn = 1
      @starting_numbers.each do |number|
        spoken_number = spoken_numbers[number]
        spoken_number.speak(current_turn)
        previous_number.record(current_turn - 1) if previous_number
        previous_number = spoken_number
        current_turn += 1
      end
      remaining_rounds = (number_of_rounds - current_turn) + 1
      remaining_rounds.times do
        previous_turn = current_turn - 1
        if previous_number.first_time?
          spoken_number = spoken_numbers[0]
          previous_number.record(previous_turn)
        else
          spoken_number = spoken_numbers[previous_turn - previous_number.last_turn_spoken]
          previous_number.record(previous_turn)
        end
        spoken_number.speak(current_turn)
        previous_number = spoken_number
        current_turn += 1
      end
    else
      previous_number = @starting_numbers[number_of_rounds - 1]
    end
    previous_number.number
  end

  class SpokenNumber
    attr_reader :last_turn_spoken, :number
    def initialize(number)
      @number = number
    end

    def first_time?
      @first_time
    end

    def speak(turn)
      if @first_time.nil?
        @first_time = true
      else
        @first_time = false
      end
    end

    def record(turn)
      @last_turn_spoken = turn
    end
  end
end
