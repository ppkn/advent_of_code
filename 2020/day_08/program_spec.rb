require "./program.rb"

RSpec.describe Program do
  let(:input) {
    <<~INPUT
      nop +0
      acc +1
      jmp +4
      acc +3
      jmp -3
      acc -99
      acc +1
      jmp -4
      acc +6
    INPUT
  }

  describe "#run!" do
    it "outputs 5" do
      expect(described_class.new(input).run!).to eq(5)
    end
  end

  describe "#fix!" do
    it "outputs 8" do
      expect(described_class.new(input).fix!).to eq(8)
    end
  end
end
