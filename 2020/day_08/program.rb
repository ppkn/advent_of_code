class Program
  Instruction = Struct.new(:operation, :direction, :value)
  attr_accessor :history

  def initialize(input)
    @counter = 0
    @accumulator = 0
    @instructions = input.split("\n").map do |line|
      line.scan(/(nop|acc|jmp) ([+-])(\d+)/)
    end
    @instructions.flatten!(1)
    @instructions.map! do |instruction|
      operation, direction, value = instruction
      Instruction.new(operation, direction, value.to_i)
    end
    @original_instructions = @instructions.map(&:dup)
  end

  def execute(instruction)
    if instruction.operation == "acc"
      @counter += 1
      if instruction.direction == "+"
        @accumulator += instruction.value
      else
        @accumulator -= instruction.value
      end
    elsif instruction.operation == "jmp"
      if instruction.direction == "+"
        @counter += instruction.value
      else
        @counter -= instruction.value
      end
    else
      @counter += 1
    end
  end

  def fix!
    reset!
    run!
    fix_instruction(@instructions.size - 1)
    @accumulator
  end

  def fix_instruction(position)
    instruction = @instructions[position]
    if instruction.operation == "nop"
      instruction.operation = "jmp"
    elsif instruction.operation == "jmp"
      instruction.operation = "nop"
    else
      fix_instruction(position - 1)
    end
    reset!
    run!
    unless @counter == @instructions.length
      reset_instructions!
      fix_instruction(position - 1)
    end
  end

  def reset_instructions!
    @instructions = @original_instructions.map(&:dup)
  end

  def reset!
    @counter = 0
    @accumulator = 0
  end

  def run!
    self.history = []
    until history.include?(@counter) || @counter >= @instructions.length
      history << @counter
      execute(@instructions[@counter])
    end
    return @accumulator
  end
end
