require "rspec"
require "./subsequent_buses"

describe "part_2" do
  let(:buses) {
    input.split(",").each_with_index.map do |id, offset|
      next if id == "x"
      Bus.new(id.to_i, offset)
    end.compact
  }

  describe "#earliest_timestamp" do
    subject { SubsequentBuses.new(buses).fast_earliest_timestamp }

    context "when input is 17,x,13,19" do
      let(:input) { "17,x,13,19" }
      it { is_expected.to eq(3417) }
    end

    context "when input is 67,7,59,61" do
      let(:input) { "67,7,59,61" }
      it { is_expected.to eq(754018) }
    end

    context "when input is 67,x,7,59,61" do
      let(:input) { "67,x,7,59,61" }
      it { is_expected.to eq(779210) }
    end

    context "when input is 67,7,x,59,61" do
      let(:input) { "67,7,x,59,61" }
      it { is_expected.to eq(1261476) }
    end

    context "when input is 1789,37,47,1889" do
      let(:input) { "1789,37,47,1889" }
      it { is_expected.to eq(1202161486) }
    end
  end
end
