class Buses
  attr_reader :buses, :start_time
  def initialize(start_time, buses)
    @start_time = start_time
    @buses = buses
  end

  def shortest_wait_time
    check_time = start_time
    loop do
      check_time += 1
      return check_time - start_time if buses.any? { |bus| (check_time % bus) == 0 }
    end
  end

  def earliest_bus_id
    check_time = start_time
    loop do
      check_time += 1
      bus_id = buses.find { |bus| (check_time % bus) == 0 }
      return bus_id if bus_id
    end
  end
end
