class SubsequentBuses
  attr_accessor :buses

  def initialize(buses)
    @buses = buses
  end

  def earliest_timestamp
    check_time = bus_with_biggest_step.id - bus_with_biggest_step.time_offset
    loop do
      check_time += bus_with_biggest_step.id
      break if buses.all? { |bus| (check_time + bus.time_offset) % bus.id == 0 }
      print("#{check_time}\r")
    end
    check_time
  end

  def fast_earliest_timestamp
    check_time = bus_with_biggest_step.id - bus_with_biggest_step.time_offset
    step_size = bus_with_biggest_step.id
    max_matches = 0
    loop do
      check_time += step_size
      matches = buses.select { |bus| (check_time + bus.time_offset) % bus.id == 0 }
      if matches.size > max_matches
        break if matches.size == buses.size
        max_matches = matches.size
        step_size = matches.map(&:id).reduce(&:*)
      end
    end
    check_time
  end

private

  def bus_with_biggest_step
    @bus_with_biggest_step ||= buses.max_by(&:id)
  end
end

Bus = Struct.new(:id, :time_offset)
