require "./buses"

start_time, buses = File.readlines("input", :chomp => true)
start_time = start_time.to_i

buses = buses.split(",").reject { |id| id == "x" }.map(&:to_i)

b = Buses.new(start_time, buses)

puts b.shortest_wait_time * b.earliest_bus_id
