require "./subsequent_buses"

_, bus_ids = File.readlines("input", :chomp => true)
buses = bus_ids.split(",").each_with_index.map do |id, offset|
  next if id == "x"
  Bus.new(id.to_i, offset)
end.compact

subs = SubsequentBuses.new(buses)
puts subs.fast_earliest_timestamp
