class JoltAdapters
  attr_accessor :adapters, :last_index
  def initialize(adapter_values)
    @adapters = adapter_values.sort
    @adapters.prepend(0)
    @adapters.append(@adapters.last + 3)
    @last_index = adapters.size - 1
  end

  def check_product_of_entire_chain
    counts = adapters.each_cons(2).reduce(Hash.new(0)) do |counter, (a, b)|
      key = b - a
      counter.merge(key => counter[key] + 1)
    end
    counts.values.reduce(:*)
  end

  def count_unique_chains
    @chain_count = Hash.new(0)
    count_chains_from(0)
  end

private

  def count_chains_from(index)
    return 1 if index == last_index
    return @chain_count[index] if @chain_count.key?(index)
    range_start = index + 1
    range_end = [last_index, index + 4].min
    (range_start..range_end).each do |test_index|
      @chain_count[index] += count_chains_from(test_index) if adapters[test_index] - adapters[index] <= 3
    end
    @chain_count[index]
  end
end
