from collections import defaultdict

class LetterCounter:
    def __init__(self, id_):
        self.id_ = id_
        self.letter_counts = defaultdict(int)

        self.bag_letters()
    
    def bag_letters(self):
        for letter in self.id_:
            self.letter_counts[letter] += 1
    
    def has_twos(self):
        return 2 in self.letter_counts.values()
    
    def has_threes(self):
        return 3 in self.letter_counts.values()

twos_count = 0
threes_count = 0
with open('input.txt', 'r') as f:
    for line in f:
        line = line.rstrip('\n')
        counter = LetterCounter(line)
        if counter.has_twos():
            twos_count += 1
        if counter.has_threes():
            threes_count += 1

print(f'Checksum is {twos_count * threes_count}')
