from itertools import combinations
with open('input.txt', 'r') as f:
    data = f.read().splitlines()

def calc_distance(str1, str2):
    return sum(char1 != char2 for char1, char2 in zip(str1, str2))

def diff_index(str1, str2):
    for i in range(len(str1)):
        if str1[i] != str2[i]:
            return i

for id_1, id_2 in combinations(data, 2):
    if calc_distance(id_1, id_2) == 1:
        index_to_delete = diff_index(id_1, id_2)
        common_string = id_1[:index_to_delete] + id_1[index_to_delete+1:]
        print(f"Common string is {common_string}")