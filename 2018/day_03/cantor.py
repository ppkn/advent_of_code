import math

def pair(k_1, k_2):
    return int(0.5 * (k_1 + k_2) * (k_1 + k_2 + 1) + k_2)

def unpair(z):
    w = math.floor((math.sqrt(8*z + 1) - 1) / 2)
    t = (w**2 + w) / 2
    y = z - t
    x = w - y
    return x, y
