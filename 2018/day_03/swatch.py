from collections import namedtuple
from operator import attrgetter
import itertools
from cantor import pair

Point = namedtuple('Point', 'x y')
class Swatch:
    def __init__(self, id_, top_left, bottom_right):
        self.id_ = id_
        self.top_left = top_left
        self.bottom_right = bottom_right
    
        self._points = None
        self._size = None

    def __and__(self, other):
        new_points = self.points & other.points
        if len(new_points) == 0:
            return None
        min_x = min(new_points, key=attrgetter('x')).x
        top_left_possibilities = [point for point in new_points if point.x == min_x]
        top_left = min(top_left_possibilities, key=attrgetter('y'))

        max_x = max(new_points, key=attrgetter('x')).x
        bottom_right_possibilities = [point for point in new_points if point.x == max_x]
        bottom_right = max(bottom_right_possibilities, key=attrgetter('y'))
        edge_x = bottom_right.x + 1
        edge_y = bottom_right.y + 1
        bottom_right = Point(edge_x, edge_y)

        id_ = pair(*sorted([self.id_, other.id_]))
        id_ += 2727
        return Swatch(id_, top_left, bottom_right)

    def __eq__(self, other):
        top_left_equal = self.top_left == other.top_left
        bottom_right_equal = self.bottom_right == other.bottom_right
        id_equal = self.id_ == other.id_
        return all([top_left_equal, bottom_right_equal, id_equal])

    def __repr__(self):
        return f'Swatch({self.id_}, {self.top_left}, {self.bottom_right})'

    def __str__(self):
        return f'#{self.id_} @ {",".join(map(str, self.top_left))}: {"x".join(map(str, self.size))}'
    

    @classmethod
    def from_string(cls, raw_string):
        id_, info = raw_string.split('@')
        id_ = int(id_.replace('#', ''))
        coordinates, size = info.split(':')
        size = [int(dim) for dim in size.split('x')]
        top_left = Point(*(int(coordinate) for coordinate in coordinates.split(',')))
        bottom_right = Point(*(point + length for point, length in zip(top_left, size)))
        return cls(id_, top_left, bottom_right)

    @property
    def points(self):
        if self._points is None:
            x_range = range(self.top_left.x, self.bottom_right.x)
            y_range = range(self.top_left.y, self.bottom_right.y)
            points_enum = itertools.product(x_range, y_range)
            self._points = set(Point(*point) for point in points_enum)
        return self._points
    
    @property
    def size(self):
        if self._size is None:
            self._size = [br_value - tl_value for tl_value, br_value in zip(self.top_left, self.bottom_right)]
        return self._size
