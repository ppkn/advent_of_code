from itertools import combinations
from functools import reduce
from operator import mul
from swatch import Swatch
from cantor import pair

with open('input.txt', 'r') as f:
    raw_swatches = f.read().splitlines()

swatches = [Swatch.from_string(swatch) for swatch in raw_swatches]
ids = {swatch.id_ for swatch in swatches}
overlapping_points = set()

for s1, s2 in combinations(swatches, 2):
    overlap = s1 & s2
    if overlap:
        ids.discard(s1.id_)
        ids.discard(s2.id_)
        overlapping_points |= {pair(*point) for point in overlap.points}

print(f"All overlapping areas total to {len(overlapping_points)}")
print(f"Set of non-overlapping swatches: {ids}")
