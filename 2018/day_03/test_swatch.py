from swatch import Swatch

def test_initialization():
    swatch = Swatch.from_string('#1 @ 1,3: 4x4')
    assert str(swatch) == '#1 @ 1,3: 4x4'

def test_overlap():
    swatch_1 = Swatch.from_string("#1 @ 1,3: 4x4")
    swatch_2 = Swatch.from_string("#2 @ 3,1: 4x4")

    overlap = Swatch.from_string('#2735 @ 3,3: 2x2')

    assert swatch_1 & swatch_2 == overlap

def test_non_overlap():
    swatch_1 = Swatch.from_string("#1 @ 0,0: 2x2")
    swatch_2 = Swatch.from_string("#2 @ 4,4: 3x4")

    assert swatch_1 & swatch_2 is None
