from itertools import cycle
with open('input.txt', 'r') as f:
    freq_change_list = f.read().splitlines()
seen_frequencies = set()
running_total = 0
for change in cycle(freq_change_list):
    running_total = eval(str(running_total) + change)
    if running_total in seen_frequencies:
        print(f'First saw {running_total} twice in changes')
        break
    else:
        seen_frequencies.add(running_total)
