class SpaceModule
  attr_reader :mass
  def initialize(mass)
    @mass = mass
  end

  def fuel_adjusted_required_fuel
    fuel_required_fuel = 0
    next_fuel_mass = Mass.new(mass.required_fuel)
    while next_fuel_mass.value > 0
      fuel_required_fuel += next_fuel_mass.required_fuel
      next_fuel_mass = Mass.new(next_fuel_mass.required_fuel)
    end
    required_fuel + fuel_required_fuel
  end

  def required_fuel
    @required_fuel ||= mass.required_fuel
  end
end

class Mass
  attr_reader :value
  def initialize(value)
    @value = value
  end

  def required_fuel
    fuel_mass_value = (value / 3).floor - 2
    [fuel_mass_value, 0].max
  end
end
