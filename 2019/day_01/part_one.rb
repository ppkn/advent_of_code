require_relative 'space_module'

total_fuel = 0
File.open('puzzle_input', 'r').each do |line|
  mass = Mass.new(line.to_i)
  total_fuel += SpaceModule.new(mass).required_fuel
end
puts "Total Fuel Required: #{total_fuel}"
