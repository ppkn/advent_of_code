require "spec_helper"
require "ship_computer"

describe ShipComputer do
  describe "#execute!" do
    subject { described_class.new(registers) }
    before { subject.execute! }
    context "when program is 1,0,0,0,99 (addition)" do
      let(:registers) { [1, 0, 0, 0, 99] }
      it "puts 2,0,0,0,99 in the registers" do
        expect(subject.registers).to eq([2, 0, 0, 0, 99])
      end
    end

    context "when program is 2,3,0,3,99 (multiplication)" do
      let(:registers) { [2, 3, 0, 3, 99] }
      it "puts 2,3,0,6,99 in the registers" do
        expect(subject.registers).to eq([2, 3, 0, 6, 99])
      end
    end

    context "when program is 2,4,4,5,99,0" do
      let(:registers) { [2, 4, 4, 5, 99, 0] }
      it "puts 2,4,4,5,99,9801 in the registers" do
        expect(subject.registers).to eq([2, 4, 4, 5, 99, 9801])
      end
    end

    context "when program is 1,1,1,4,99,5,6,0,99" do
      let(:registers) { [1, 1, 1, 4, 99, 5, 6, 0, 99] }
      it "puts 30,1,1,4,2,5,6,0,99 in the registers" do
        expect(subject.registers).to eq([30, 1, 1, 4, 2, 5, 6, 0, 99])
      end
    end
  end
end
