require "spec_helper"
require "space_module"

describe SpaceModule do
  let(:mass) { Mass.new(mass_value) }
  describe "#required_fuel" do
    subject { described_class.new(mass).required_fuel }
    context "with a mass_value of 12" do
      let(:mass_value) { 12 }
      it { is_expected.to eq(2) }
    end

    context "with a mass_value of 14" do
      let(:mass_value) { 14 }
      it { is_expected.to eq(2) }
    end

    context "with a mass_value of 1969" do
      let(:mass_value) { 1969 }
      it { is_expected.to eq(654) }
    end

    context "with a mass_value of 100756" do
      let(:mass_value) { 100756 }
      it { is_expected.to eq(33583) }
    end
  end

  describe "#fuel_adjusted_required_fuel" do
    subject { described_class.new(mass).fuel_adjusted_required_fuel }
    context "with a mass_value of 14" do
      let(:mass_value) { 14 }
      it { is_expected.to eq(2) }
    end

    context "with a mass_value of " do
      let(:mass_value) { 1969 }
      it { is_expected.to eq(966) }
    end

    context "with a mass_value of 14" do
      let(:mass_value) { 100756 }
      it { is_expected.to eq(50346) }
    end
  end
end
