require_relative "ship_computer"

puzzle_input = File.read("puzzle_input").split(",").map(&:to_i)
computer = ShipComputer.new(puzzle_input)
computer.set_register(1, 12)
computer.set_register(2, 2)
computer.execute!

puts "The zero register contains #{computer.registers[0]} after program ran"
