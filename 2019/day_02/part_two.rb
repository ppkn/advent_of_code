require "random"
require "set"
require_relative "ship_computer"

puzzle_input = File.read("puzzle_input").split(",").map(&:to_i)
tried_pairs = Set.new

def generate_pair
  tries = 0
  loop do
    pair = [Random.rand(100), Random.rand(100)]
    unless tried_pairs.include?(pair)
      tried_pairs << pair
      return pair
    end
    tries += 1
    raise "tried too many pairs" if tries > 10000
  end
end

tried_count = 0
loop do
  pair = generate_pair
  break unless pair
  computer = ShipComputer.new(puzzle_input)
  computer.set_register(1, pair[0])
  computer.set_register(2, pair[1])
  computer.execute!
  print ""
  if computer.registers[0] == 19690720
    puts "#{pair} evaluates to 19690720"
    break
  end
end
