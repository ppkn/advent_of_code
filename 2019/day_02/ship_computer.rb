class ShipComputer
  attr_reader :registers

  def initialize(register_values)
    @instruction_counter = 0
    @end_of_program = false
    @registers = []
    register_values.each_with_index do |value, address|
      @registers[address] = value
    end
  end

  def execute!
    until end_of_program
      execute_current_instruction
      @instruction_counter += 4
    end
  end

  def set_register(address, value)
    @registers[address] = value
  end

  class OperationError < StandardError
  end

private

  attr_reader :instruction_counter,
              :end_of_program

  def execute_current_instruction
    opcode = registers[instruction_counter]
    case opcode
    when 1
      register_one = registers[instruction_counter + 1]
      register_two = registers[instruction_counter + 2]
      output_register = registers[instruction_counter + 3]
      op1(register_one, register_two, output_register)
    when 2
      register_one = registers[instruction_counter + 1]
      register_two = registers[instruction_counter + 2]
      output_register = registers[instruction_counter + 3]
      op2(register_one, register_two, output_register)
    when 99
      op99
    else
      raise OperationError, "#{opcode} at address #{instruction_counter} is an invalid opcode."
    end
  end

  def op1(register_a_position, register_b_position, output_register_position)
    @registers[output_register_position] = registers[register_a_position] + registers[register_b_position]
  end

  def op2(register_a_position, register_b_position, output_register_position)
    @registers[output_register_position] = registers[register_a_position] * registers[register_b_position]
  end

  def op99
    @end_of_program = true
  end
end
